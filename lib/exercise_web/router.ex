defmodule ExerciseWeb.Router do
  use ExerciseWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ExerciseWeb do
    pipe_through :api

    post "/operations", OperationController, :create
    get "/balance/:account_number", BalanceController, :show
    get "/statement/:account_number", StatementController, :show
    get "/debt_period/:account_number", DebtPeriodController, :show
  end
end
