defmodule ExerciseWeb.DebtPeriodController do
  use ExerciseWeb, :controller

  @moduledoc """
  # Compute periods of debt.

  Create a HTTP endpoint which returns the periods which the account's balance
  was negative, i.e, periods when the bank can charge interest on that account.

  E.g: if we have three more operations (current balance is 771.43):
  Purchase of a flight ticket 800.00 at 18/10
  Purchase of a espresso 10.00 at 22/10
  Deposit 100.00 at 25/10

  The endpoint would return:
  - Principal: 28.57
    Start: 18/10
    End: 21/10

  - Principal: 38.57
    Start: 22/10
    End: 24/10

  This endpoint should return multiple periods, if applicable, and omit the "End:"
  date if the account's balance is currently negative.
  """
  def show(conn, %{"account_number" => account_number} = _params) do
    {status_code, response} =
      case Exercise.debts_by_date(account_number) do
        {:ok, debts} -> {200, debts}
        {:error, messages} -> {422, %{errors: messages}}
      end

    conn
    |> put_status(status_code)
    |> json(response)
  end
end
