defmodule ExerciseWeb.OperationController do
  use ExerciseWeb, :controller

  @moduledoc """
  # Adding the operations on that checking account

  Create a HTTP service in which you can add an operation to a given checking account, identified by the account number. This operation will contain the account number, a short description, an amount and the date it happened. Keep in mind you have to support both credit and debit operations, i.e, both putting and taking money out of the account.

  E.g.:
    Deposit 1000.00 at 15/10
    Purchase on Amazon 3.34 at 16/10
    Purchase on Uber 45.23 at 16/10
    Withdrawal 180.00 at 17/10

  Deposits can take days to be acknowledged properly, so you should support insertion in any date order.
  """
  def create(conn, params) do
    {status_code, response} =
      case Exercise.create_operation(params) do
        {:ok, operation} -> {200, operation}
        {:error, messages} -> {422, %{errors: messages}}
      end

    conn
    |> put_status(status_code)
    |> json(response)
  end
end
