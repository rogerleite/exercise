defmodule ExerciseWeb.BalanceController do
  use ExerciseWeb, :controller

  @moduledoc """
  # Get the current balance

  Create a HTTP endpoint which returns the current balance of a given account.
  This balance is the sum of all operations until today, so the customer can
  know how much money they still have.

  E.g: for the sample above, the customer would have
  1000.00 - 3.34 - 45.23 - 180.00 = 771.43
  """
  def show(conn, %{"account_number" => account_number} = _params) do
    {status_code, response} =
      case Exercise.current_balance(account_number) do
        {:ok, balance} -> {200, balance}
        {:error, messages} -> {422, %{errors: messages}}
      end

    conn
    |> put_status(status_code)
    |> json(response)
  end
end
