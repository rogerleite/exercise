defmodule ExerciseWeb.StatementController do
  use ExerciseWeb, :controller

  @moduledoc """
  # Third step: Get the bank statement

  Create an HTTP endpoint which returns the bank statement of an account given a period of dates. This statement will contain the operations of each day and the balance at the end of each day.
  E.g:

  15/10:
  - Deposit 1000.00
  Balance: 1000.00

  16/10:
  - Purchase on Amazon 3.34
  - Purchase on Uber 45.23
  Balance: 951.43

  17/10:
  - Withdrawal 180.00
  Balance: 771.43
  """
  def show(conn, params) do
    {status_code, response} =
      case Exercise.balances_by_date(params) do
	{:ok, statement} -> {200, statement}
	{:error, messages} -> {422, %{errors: messages}}
      end

    conn
    |> put_status(status_code)
    |> json(response)
  end
end
