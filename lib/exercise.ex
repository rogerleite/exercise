defmodule Exercise do
  alias Exercise.Balance
  alias Exercise.DebtPeriod
  alias Exercise.MemoryServer
  alias Exercise.Operation
  alias Exercise.Statement

  def create_operation(params) do
    case Operation.cast_and_validate(params) do
      {:ok, operation} ->
        MemoryServer.add_operation(operation)
        {:ok, operation |> Operation.serialize}

      {:invalid_operation, reasons} ->
        {:error, reasons}
    end
  end

  def current_balance(account_number) do
    account_number
    |> Balance.current_balance(MemoryServer.operations())
  end

  def balances_by_date(params) do
    case Statement.cast_and_validate(params) do
      {:ok, statement} ->
        {:ok, Statement.balances_by_date(statement, MemoryServer.operations())}

      {:error, messages} ->
        {:error, messages}
    end
  end

  def debts_by_date(account_number) do
    account_number
    |> DebtPeriod.debts_by_date(MemoryServer.operations())
  end
end
