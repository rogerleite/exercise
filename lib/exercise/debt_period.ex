defmodule Exercise.DebtPeriod do

  alias Exercise.Operation

  def debts_by_date(account_number, operations) when is_binary(account_number) do
    case Operation.parse_account_number(account_number) do
      {:ok, parsed_account_number} ->
        debts_by_date(parsed_account_number, operations)
      {:error, message} ->
        {:error, [message]}
    end
  end

  def debts_by_date(account_number, all_operations) when is_integer(account_number) do
    calculate_balance_by_date = fn {date, operations} ->
      balance =
        operations
        |> Enum.reduce(0.0, fn operation, total -> total + operation.amount end)
        |> Float.round(2)

      %{date: date, balance: balance}
    end

    calculate_balances = fn date_structure, acc_balance ->
      balance = (acc_balance + date_structure.balance) |> Float.round(2)
      new_struct = %{date_structure | balance: balance}
      {new_struct, balance}
    end

    {daily_balances, _balances} =
      all_operations
      |> Enum.filter(&(&1.account_number == account_number))
      |> Enum.group_by(& &1.date)
      |> Enum.sort(&Operation.sort_by_date/2)
      |> Enum.map(calculate_balance_by_date)
      |> Enum.map_reduce(0.0, calculate_balances)

    debts_list =
      daily_balances
      |> Enum.map_reduce([], &calculate_debt_periods/2)
      |> (fn {_daily_balance, debts} -> debts |> Enum.reverse() end).()

    result = %{
      account_number: account_number,
      debts: debts_list
    }

    {:ok, result}
  end

  defp calculate_debt_periods(daily_balance, debts) do
    {previous_debt, debts_tail} = debts |> List.pop_at(0)

    new_debts =
      case handle_debt(daily_balance, previous_debt) do
        {:insert, new_debt} ->
          [new_debt | debts]

        {:update, updated_debt} ->
          [updated_debt | debts_tail]

        {:update_and_insert, updated_debt, new_debt} ->
          [new_debt | [updated_debt | debts_tail]]

        {:skip, _debt} ->
          debts
      end

    {daily_balance, new_debts}
  end

  defp handle_debt(%{balance: balance, date: date} = _daily_balance, nil = _previous_debt) do
    cond do
      balance < 0.0 ->
        new_debt = %{principal: balance * -1, start_date: date |> Date.to_string(), end_date: nil}
        {:insert, new_debt}

      true ->
        {:skip, nil}
    end
  end

  defp handle_debt(%{balance: balance, date: date} = daily_balance, previous_debt) do
    cond do
      balance > 0.0 && previous_debt.end_date == nil ->
        updated_debt = %{previous_debt | end_date: date |> Date.add(-1) |> Date.to_string()}
        {:update, updated_debt}

      balance < 0.0 && previous_debt.end_date == nil ->
        updated_debt = %{previous_debt | end_date: date |> Date.add(-1) |> Date.to_string()}
        {:insert, new_debt} = handle_debt(daily_balance, nil)
        {:update_and_insert, updated_debt, new_debt}

      true ->
        {:skip, nil}
    end
  end
end
