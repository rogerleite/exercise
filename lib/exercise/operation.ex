defmodule Exercise.Operation do

  @derive {Jason.Encoder, only: [:account_number, :description, :amount, :date]}

  defstruct [
    :account_number,
    :description,
    :amount,
    :date
  ]

  def cast_and_validate(params) do
    case validate_keys_presence(params) do
      {:ok, params} ->
        build_operation(params)

      {:invalid_operation, reasons} ->
        {:invalid_operation, reasons}
    end
  end

  def serialize(%__MODULE__{} = op) do
    %{
      account_number: op.account_number,
      description: op.description,
      amount: :io_lib.format("~.2f", [op.amount]) |> IO.iodata_to_binary,
      date: op.date |> Date.to_string
    }
  end

  def parse_account_number(account_number) do
    case Integer.parse(account_number |> to_string) do
      {number, _} ->
        {:ok, number}
      _ ->
        {:error, "Invalid account number"}
    end
  end

  def sort_by_date({date1, _}, {date2, _}) do
    case Date.compare(date1, date2) do
      :lt -> true
      _ -> false
    end
  end

  defp validate_keys_presence(params) do
    required_keys = ["account_number", "amount", "date", "description"]
    params_keys = params |> Map.keys()

    case Enum.all?(required_keys, fn key -> key in params_keys end) do
      true ->
        {:ok, params}

      false ->
        {:invalid_operation,
         ["Invalid params. account_number, description, amount and date are required."]}
    end
  end

  defp build_operation(
         %{
           "account_number" => account_number,
           "description" => description,
           "amount" => amount,
           "date" => date
         } = _params
       ) do

    {parsed_account_number, all_validations} =
      case parse_account_number(account_number) do
        {:ok, parsed_account_number} ->
          {parsed_account_number, []}
        {:error, message} ->
          {nil, [message]}
      end

    parsed_description = description |> to_string |> String.trim()

    invalid_reason =
      case parsed_description do
        "" -> "description is required"
        _ -> nil
      end

    all_validations = [invalid_reason | all_validations]

    {parsed_amount, invalid_reason} =
      case Float.parse(amount |> to_string) do
        {number, _} -> {number, nil}
        _ -> {nil, "Invalid amount"}
      end

    all_validations = [invalid_reason | all_validations]

    {parsed_date, invalid_reason} =
      case Date.from_iso8601(date |> to_string) do
        {:ok, date} -> {date, nil}
        {:error, :invalid_format} -> {nil, "Invalid date format"}
        {:error, :invalid_date} -> {nil, "Invalid date"}
      end

    all_validations = [invalid_reason | all_validations]

    validations = all_validations |> Enum.reject(&is_nil/1)
    is_valid = validations |> Enum.empty?()

    if is_valid do
      operation = %__MODULE__{
        account_number: parsed_account_number,
        description: parsed_description,
        amount: parsed_amount,
        date: parsed_date
      }

      {:ok, operation}
    else
      {:invalid_operation, validations}
    end
  end
end
