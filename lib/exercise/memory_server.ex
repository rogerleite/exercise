defmodule Exercise.MemoryServer do
  use GenServer

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, :empty, name: __MODULE__)
  end

  def operations do
    GenServer.call(__MODULE__, :operations)
  end

  def add_operation(%Exercise.Operation{} = operation) do
    GenServer.call(__MODULE__, {:add_operation, operation})
  end

  def clear do
    GenServer.call(__MODULE__, :clear)
  end

  @impl true
  def init(:empty) do
    {
      :ok,
      %{operations: []}
    }
  end

  @impl true
  def handle_call(:operations, _from, state) do
    {:reply, state[:operations], state}
  end

  @impl true
  def handle_call({:add_operation, operation}, _from, state) do
    new_state =
      state
      |> Map.put(:operations, [operation | state[:operations]])

    {:reply, operation, new_state}
  end

  @impl true
  def handle_call(:clear, _from, state) do
    new_state =
      state
      |> Map.put(:operations, [])

    {:reply, true, new_state}
  end
end
