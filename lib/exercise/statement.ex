defmodule Exercise.Statement do
  defstruct [
    :account_number,
    :start_date,
    :end_date
  ]

  alias Exercise.Operation

  def cast_and_validate(params) do
    case validate_keys_presence(params) do
      {:ok, params} ->
        build_statement(params)

      {:error, messages} ->
        {:error, messages}
    end
  end

  def balances_by_date(%__MODULE__{} = statement, all_operations) do
    period_range = Date.range(statement.start_date, statement.end_date)

    filter_by_account_number_and_period_date = fn(operation) ->
      operation.account_number == statement.account_number &&
        Enum.member?(period_range, operation.date)
    end

    build_date_structure = fn {date, operations} ->
      formatted_operations =
        operations
        |> Enum.map(fn op ->
          %{
            description: op.description,
            amount: :io_lib.format("~.2f", [op.amount]) |> IO.iodata_to_binary
          }
        end)

      balance =
        operations
        |> Enum.reduce(0.0, fn(operation, total) -> total + operation.amount end)
        |> Float.round(2)

      %{
        date: date,
        operations: formatted_operations,
        balance: balance
      }
    end

    calculate_balances = fn(date_structure, acc_balance) ->
      balance =
        acc_balance + date_structure.balance
        |> Float.round(2)
      formatted_balance = :io_lib.format("~.2f", [balance]) |> IO.iodata_to_binary

      new_struct = %{date_structure | balance: formatted_balance}
      {new_struct, balance}
    end

    operations_by_date =
      all_operations
      |> Enum.filter(filter_by_account_number_and_period_date)
      |> Enum.group_by(& &1.date)
      |> Enum.sort(&Operation.sort_by_date/2)
      |> Enum.map(build_date_structure)

    {dates, _balance} =
      operations_by_date
      |> Enum.map_reduce(0.0, calculate_balances)

    %{
      account_number: statement.account_number,
      start_date: statement.start_date,
      end_date: statement.end_date,
      dates: dates
    }
  end

  defp validate_keys_presence(params) do
    required_keys = ["account_number", "start_date", "end_date"]
    params_keys = params |> Map.keys()

    case Enum.all?(required_keys, fn key -> key in params_keys end) do
      true ->
        {:ok, params}

      false ->
        {:error, ["Invalid params. account_number, start_date and end_date are required."]}
    end
  end

  defp build_statement(
         %{
           "account_number" => account_number,
           "start_date" => start_date,
           "end_date" => end_date
         } = _params
       ) do

    {parsed_account_number, all_validations} =
      case Operation.parse_account_number(account_number) do
        {:ok, parsed_account_number} ->
          {parsed_account_number, []}
        {:error, message} ->
          {nil, [message]}
      end

    {parsed_start_date, invalid_reason} =
      case Date.from_iso8601(start_date |> to_string) do
        {:ok, date} -> {date, nil}
        {:error, :invalid_format} -> {nil, "Invalid start date format"}
        {:error, :invalid_date} -> {nil, "Invalid start date"}
      end

    all_validations = [invalid_reason | all_validations]

    {parsed_end_date, invalid_reason} =
      case Date.from_iso8601(end_date |> to_string) do
        {:ok, date} -> {date, nil}
        {:error, :invalid_format} -> {nil, "Invalid end date format"}
        {:error, :invalid_date} -> {nil, "Invalid end date"}
      end

    all_validations = [invalid_reason | all_validations]

    invalid_reason =
      if parsed_start_date != nil && parsed_end_date != nil do
        case Date.compare(parsed_start_date, parsed_end_date) do
          :gt -> "Start date can't be greater than end date"
          _ -> nil
        end
      end

    all_validations = [invalid_reason | all_validations]

    validations = all_validations |> Enum.reject(&is_nil/1)
    is_valid = validations |> Enum.empty?()

    if is_valid do
      {:ok,
       %__MODULE__{
         account_number: parsed_account_number,
         start_date: parsed_start_date,
         end_date: parsed_end_date
       }}
    else
      {:error, validations}
    end
  end
end
