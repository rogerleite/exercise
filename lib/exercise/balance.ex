defmodule Exercise.Balance do

  alias Exercise.Operation

  def current_balance(account_number, operations) when is_binary(account_number) do
    case Operation.parse_account_number(account_number) do
      {:ok, parsed_account_number} ->
        current_balance(parsed_account_number, operations)
      {:error, message} ->
        {:error, [message]}
    end
  end

  def current_balance(account_number, []) when is_integer(account_number) do
    {:ok, %{account_number: account_number, balance: 0.0}}
  end

  def current_balance(account_number, all_operations) when is_integer(account_number) do
    filter_by_account_number_and_current_date =
      fn(operation) ->
        date_compare = Date.compare(operation.date, Date.utc_today())
        operation.account_number == account_number && date_compare in [:lt, :eq]
      end

    balance =
      all_operations
      |> Enum.filter(filter_by_account_number_and_current_date)
      |> Enum.reduce(0.0, fn operation, total -> total + operation.amount end)
      |> Float.round(2)

    {:ok, %{account_number: account_number, balance: balance}}
  end
end
