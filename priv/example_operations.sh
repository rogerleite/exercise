
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"account_number":123,"description":"Deposit","amount":1000,"date":"2019-04-15"}' \
  http://localhost:4000/operations
echo

curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"account_number":123,"description":"Purchase on Amazon","amount":-3.34,"date":"2019-04-16"}' \
  http://localhost:4000/operations
echo

curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"account_number":123,"description":"Purchase on Uber","amount":-45.23,"date":"2019-04-16"}' \
  http://localhost:4000/operations
echo

curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"account_number":123,"description":"Withdrawal","amount":-180,"date":"2019-04-17"}' \
  http://localhost:4000/operations
echo

curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"account_number":123,"description":"Purchase of a flight ticket","amount":-800,"date":"2019-04-18"}' \
  http://localhost:4000/operations
echo

curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"account_number":123,"description":"Purchase of a espresso","amount":-10,"date":"2019-04-22"}' \
  http://localhost:4000/operations
echo

curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"account_number":123,"description":"Deposit","amount":100,"date":"2019-04-25"}' \
  http://localhost:4000/operations
echo

