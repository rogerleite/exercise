defmodule ExerciseTest do
  use ExUnit.Case, async: true
  import Exercise.DataHelpers

  alias Exercise.MemoryServer

  setup :memory_server_clear

  describe "create_operation" do
    test "returns error for invalid params" do
      invalid_params = %{invalid_key: "invalid_value"}

      assert Exercise.create_operation(invalid_params) ==
               {:error,
                ["Invalid params. account_number, description, amount and date are required."]}
    end

    test "persist in memory for valid params" do
      valid_params = %{
        "account_number" => "123",
        "description" => "Deposit",
        "amount" => 1000,
        "date" => "2019-04-15"
      }

      assert {:ok, operation} = Exercise.create_operation(valid_params)
      assert operation.account_number == 123
      assert operation.description == "Deposit"
      assert operation.amount == "1000.00"
      assert operation.date == "2019-04-15"

      assert MemoryServer.operations() == [
        %Exercise.Operation{
          account_number: 123,
          description: "Deposit",
          amount: 1000.0,
          date: ~D[2019-04-15]
        }
      ]
    end
  end

  describe "current_balance" do
    test "returns error for invalid account number" do
      assert Exercise.current_balance("not-a-number-account") ==
               {:error, ["Invalid account number"]}
    end

    test "returns zero for account number without operations" do
      assert Exercise.current_balance("123") ==
               {:ok, %{account_number: 123, balance: 0.0}}
    end

    test "returns sum of all operations until today" do
      memory_server_add_operations([
        build_operation(123, "Deposit", 200.0, Date.utc_today() |> Date.add(-1)),
        build_operation(123, "Withdrawal", -180.99, Date.utc_today()),
        build_operation(123, "Credit on future", 200.0, Date.utc_today() |> Date.add(1))
      ])

      assert Exercise.current_balance("123") ==
               {:ok, %{account_number: 123, balance: 19.01}}
    end
  end

  describe "balances_by_date" do
    test "returns error for invalid account number" do
      params = %{
        "account_number" => "XxX",
        "start_date" => "2019-04-15",
        "end_date" => "2019-04-15"
      }

      assert Exercise.balances_by_date(params) ==
               {:error, ["Invalid account number"]}
    end

    test "returns zero for account number without operations" do
      params = %{
        "account_number" => "123",
        "start_date" => "2019-04-15",
        "end_date" => "2019-04-15"
      }

      assert Exercise.balances_by_date(params) ==
               {:ok,
                %{
                  account_number: 123,
                  start_date: ~D[2019-04-15],
                  end_date: ~D[2019-04-15],
                  dates: []
                }}
    end

    test "returns the operations of each day and the balance" do
      start_date = Date.utc_today() |> Date.add(-30)
      end_date = Date.utc_today()

      memory_server_add_operations([
        build_operation(123, "Deposit", 200.0, start_date |> Date.add(-1)),
        build_operation(123, "Deposit", 200.0, start_date),
        build_operation(123, "Withdrawal", -180.00, end_date),
        build_operation(123, "Credit", 200.0, end_date),
        build_operation(321, "Deposit", 200.0, start_date)
      ])

      params = %{
        "account_number" => "123",
        "start_date" => start_date |> Date.to_string(),
        "end_date" => end_date |> Date.to_string()
      }

      assert {:ok, result} = Exercise.balances_by_date(params)

      assert result.account_number == 123
      assert result.start_date == start_date
      assert result.end_date == end_date
      # start_date, end_date
      assert Enum.count(result.dates) == 2

      assert {date0, _list} = result.dates |> List.pop_at(0)

      assert date0 == %{
               date: start_date,
               operations: [
                 %{description: "Deposit", amount: "200.00"}
               ],
               balance: "200.00"
             }

      assert {date1, _list} = result.dates |> List.pop_at(1)

      assert date1 == %{
               date: end_date,
               operations: [
                 %{description: "Credit", amount: "200.00"},
                 %{description: "Withdrawal", amount: "-180.00"}
               ],
               balance: "220.00"
             }
    end
  end

  describe "debts_by_date" do
    test "returns error for invalid account number" do
      assert Exercise.debts_by_date("xxx") ==
               {:error, ["Invalid account number"]}
    end

    test "returns debts empty for account number without operations" do
      assert Exercise.debts_by_date("123") ==
               {:ok, %{account_number: 123, debts: []}}
    end

    test "returns periods when the bank can charge interest" do
      memory_server_add_operations([
        build_operation(321, "Deposit", 199.0, ~D[2019-04-15]),
        build_operation(123, "Deposit", 1000, ~D[2019-04-15]),
        build_operation(123, "Purchase on Amazon", -3.34, ~D[2019-04-16]),
        build_operation(123, "Purchase on Uber", -45.23, ~D[2019-04-16]),
        build_operation(123, "Withdrawal", -180.0, ~D[2019-04-17]),
        build_operation(123, "Purchase of a flight ticket", -800.0, ~D[2019-04-18]),
        build_operation(123, "Purchase of a espresso", -10.0, ~D[2019-04-22]),
        build_operation(123, "Deposit", 100.0, ~D[2019-04-25])
      ])

      assert {:ok, result} = Exercise.debts_by_date(123)

      assert result.account_number == 123
      assert result.debts |> Enum.count() == 2

      assert {period0, _list} = result.debts |> List.pop_at(0)

      assert period0 == %{
               principal: 28.57,
               start_date: "2019-04-18",
               end_date: "2019-04-21"
             }

      assert {period1, _list} = result.debts |> List.pop_at(1)

      assert period1 == %{
               principal: 38.57,
               start_date: "2019-04-22",
               end_date: "2019-04-24"
             }
    end
  end
end
