defmodule Exercise.DataHelpers do

  alias Exercise.MemoryServer
  alias Exercise.Operation

  def memory_server_clear(_context) do
    MemoryServer.clear()
    :ok
  end

  def build_operation(account_number, description, amount, date) do
    %Operation{
      account_number: account_number,
      description: description,
      amount: amount,
      date: date
    }
  end

  def memory_server_add_operations(operations) when is_list(operations) do
    operations
    |> Enum.each(&MemoryServer.add_operation/1)
  end
end
