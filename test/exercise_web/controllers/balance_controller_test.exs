defmodule ExerciseWeb.BalanceControllerTest do
  use ExerciseWeb.ConnCase
  import Exercise.DataHelpers

  setup :memory_server_clear

  describe "show/2" do
    test "shows balance zero for account number without operations" do
      response =
        build_conn()
        |> get(Routes.balance_path(build_conn(), :show, "321"))
        |> json_response(200)

      assert response == %{
               "account_number" => 321,
               "balance" => 0.0
             }
    end

    test "shows balance for account with operations" do
      memory_server_add_operations([
        build_operation(123, "Deposit", 200.0, Date.utc_today() |> Date.add(-1)),
        build_operation(123, "Withdrawal", -180.99, Date.utc_today()),
        build_operation(123, "Credit on future", 200.0, Date.utc_today() |> Date.add(1))
      ])

      response =
        build_conn()
        |> get(Routes.balance_path(build_conn(), :show, "123"))
        |> json_response(200)

      assert response == %{
               "account_number" => 123,
               "balance" => 19.01
             }
    end

    test "returns a validation error on invalid account number" do
      response =
        build_conn()
        |> get(Routes.balance_path(build_conn(), :show, "not-a-number-account"))
        |> json_response(422)

      assert response == %{"errors" => ["Invalid account number"]}
    end
  end
end
