defmodule ExerciseWeb.OperationControllerTest do
  use ExerciseWeb.ConnCase

  describe "create/2" do
    test "creates a credit operation" do
      valid_params = %{
        "account_number" => "123",
        "description" => "Deposit",
        "amount" => 1000,
        "date" => "2019-04-15"
      }

      response =
        build_conn()
        |> post(Routes.operation_path(build_conn(), :create), valid_params)
        |> json_response(200)

      assert response == %{
        "account_number" => 123,
        "amount" => "1000.00",
        "date" => "2019-04-15",
        "description" => "Deposit"
      }
    end

    test "creates a debit operation" do
      valid_params = %{
        "account_number" => "123",
        "description" => "Purchase on Amazon",
        "amount" => -3.34,
        "date" => "2019-04-16"
      }

      response =
        build_conn()
        |> post(Routes.operation_path(build_conn(), :create), valid_params)
        |> json_response(200)

      assert response == %{
                 "account_number" => 123,
                 "amount" => "-3.34",
                 "date" => "2019-04-16",
                 "description" => "Purchase on Amazon"
             }
    end

    test "returns a validation error on invalid attributes" do
      invalid_params = %{invalid_key: "invalid_value"}

      response =
        build_conn()
        |> post(Routes.operation_path(build_conn(), :create), invalid_params)
        |> json_response(422)

      assert response == %{
               "errors" => [
                 "Invalid params. account_number, description, amount and date are required."
               ]
             }
    end
  end
end
