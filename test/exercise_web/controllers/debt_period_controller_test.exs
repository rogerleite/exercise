defmodule ExerciseWeb.DebtPeriodControllerTest do
  use ExerciseWeb.ConnCase
  import Exercise.DataHelpers

  setup :memory_server_clear

  describe "show/2" do
    test "returns periods when the bank can charge interest" do
      memory_server_add_operations([
        build_operation(321, "Deposit", 199.0, ~D[2019-04-15]),
        build_operation(123, "Deposit", 1000, ~D[2019-04-15]),
        build_operation(123, "Purchase on Amazon", -3.34, ~D[2019-04-16]),
        build_operation(123, "Purchase on Uber", -45.23, ~D[2019-04-16]),
        build_operation(123, "Withdrawal", -180.0, ~D[2019-04-17]),
        build_operation(123, "Purchase of a flight ticket", -800.0, ~D[2019-04-18]),
        build_operation(123, "Purchase of a espresso", -10.0, ~D[2019-04-22]),
        build_operation(123, "Deposit", 100.0, ~D[2019-04-25])
      ])

      response =
        build_conn()
        |> get(Routes.debt_period_path(build_conn(), :show, "123"))
        |> json_response(200)

      assert response == %{
               "account_number" => 123,
               "debts" => [
                 %{
                   "end_date" => "2019-04-21",
                   "principal" => 28.57,
                   "start_date" => "2019-04-18"
                 },
                 %{
                   "end_date" => "2019-04-24",
                   "principal" => 38.57,
                   "start_date" => "2019-04-22"
                 }
               ]
             }
    end

    test "returns a validation error on invalid account number" do
      response =
        build_conn()
        |> get(Routes.debt_period_path(build_conn(), :show, "not-a-number-account"))
        |> json_response(422)

      assert response == %{"errors" => ["Invalid account number"]}
    end
  end
end
