defmodule ExerciseWeb.StatementControllerTest do
  use ExerciseWeb.ConnCase
  import Exercise.DataHelpers

  setup :memory_server_clear

  describe "show/2" do
    test "returns zero for account number without operations" do
      start_date = Date.utc_today() |> Date.add(-30)
      end_date = Date.utc_today()

      valid_params = %{
        "start_date" => start_date |> Date.to_string(),
        "end_date" => end_date |> Date.to_string()
      }

      response =
        build_conn()
        |> get(Routes.statement_path(build_conn(), :show, 123), valid_params)
        |> json_response(200)

      assert response == %{
               "account_number" => 123,
               "start_date" => start_date |> Date.to_string(),
               "end_date" => end_date |> Date.to_string(),
               "dates" => []
             }
    end

    test "returns the operations of each day and the balance" do
      start_date = Date.utc_today() |> Date.add(-30)
      end_date = Date.utc_today()

      valid_params = %{
        "start_date" => start_date |> Date.to_string(),
        "end_date" => end_date |> Date.to_string()
      }

      memory_server_add_operations([
        build_operation(123, "Deposit", 200.0, start_date),
        build_operation(123, "Withdrawal", -180.00, end_date)
      ])

      response =
        build_conn()
        |> get(Routes.statement_path(build_conn(), :show, "123"), valid_params)
        |> json_response(200)

      assert response == %{
               "account_number" => 123,
               "start_date" => valid_params["start_date"],
               "end_date" => valid_params["end_date"],
               "dates" => [
                 %{
                   "balance" => "200.00",
                   "date" => valid_params["start_date"],
                   "operations" => [%{"amount" => "200.00", "description" => "Deposit"}]
                 },
                 %{
                   "balance" => "20.00",
                   "date" => valid_params["end_date"],
                   "operations" => [%{"amount" => "-180.00", "description" => "Withdrawal"}]
                 }
               ]
             }
    end

    test "returns a validation error on invalid attributes" do
      invalid_params = %{invalid_key: "invalid_value"}

      response =
        build_conn()
        |> get(Routes.statement_path(build_conn(), :show, "non-exist"), invalid_params)
        |> json_response(422)

      assert response == %{
               "errors" => [
                 "Invalid params. account_number, start_date and end_date are required."
               ]
             }
    end
  end
end
