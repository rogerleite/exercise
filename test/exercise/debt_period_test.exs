defmodule Exercise.DebtPeriodTest do
  use ExUnit.Case, async: true
  import Exercise.DataHelpers

  alias Exercise.DebtPeriod

  describe "debts_by_date" do
    test "validates account number is number" do
      assert DebtPeriod.debts_by_date("xxx", []) == {:error, ["Invalid account number"]}
    end

    test "returns debts empty for account without operations" do
      operations = [
        build_operation(321, "Deposit", 199.0, ~D[2019-04-15])
      ]

      assert {:ok, result} = DebtPeriod.debts_by_date(123, operations)
      assert result.account_number == 123
      assert result.debts == []
    end

    test "returns debts empty for account with credit operations only" do
      operations = [
        build_operation(123, "Deposit", 1000, ~D[2019-04-15]),
        build_operation(123, "Deposit", 100.0, ~D[2019-04-25])
      ]

      assert {:ok, result} = DebtPeriod.debts_by_date(123, operations)
      assert result.account_number == 123
      assert result.debts == []
    end

    test "returns periods when the bank can charge interest" do
      operations = [
        build_operation(321, "Deposit", 199.0, ~D[2019-04-15]),
        build_operation(123, "Deposit", 1000, ~D[2019-04-15]),
        build_operation(123, "Purchase on Amazon", -3.34, ~D[2019-04-16]),
        build_operation(123, "Purchase on Uber", -45.23, ~D[2019-04-16]),
        build_operation(123, "Withdrawal", -180.0, ~D[2019-04-17]),
        build_operation(123, "Purchase of a flight ticket", -800.0, ~D[2019-04-18]),
        build_operation(123, "Purchase of a espresso", -10.0, ~D[2019-04-22]),
        build_operation(123, "Deposit", 100.0, ~D[2019-04-25])
      ]

      assert {:ok, result} = DebtPeriod.debts_by_date(123, operations)
      assert result.account_number == 123

      assert {period0, _list} = result.debts |> List.pop_at(0)

      assert period0 == %{
               principal: 28.57,
               start_date: "2019-04-18",
               end_date: "2019-04-21"
             }

      assert {period1, _list} = result.debts |> List.pop_at(1)

      assert period1 == %{
               principal: 38.57,
               start_date: "2019-04-22",
               end_date: "2019-04-24"
             }
    end

    test "returns periods when the bank can charge interest (end date open)" do
      operations = [
        build_operation(321, "Deposit", 199.0, ~D[2019-04-15]),
        build_operation(123, "Deposit", 1000, ~D[2019-04-15]),
        build_operation(123, "Purchase on Amazon", -3.34, ~D[2019-04-16]),
        build_operation(123, "Purchase on Uber", -45.23, ~D[2019-04-16]),
        build_operation(123, "Withdrawal", -180.0, ~D[2019-04-17]),
        build_operation(123, "Purchase of a flight ticket", -800.0, ~D[2019-04-18]),
        build_operation(123, "Purchase of a espresso", -10.0, ~D[2019-04-22])
      ]

      assert {:ok, result} = DebtPeriod.debts_by_date(123, operations)
      assert result.account_number == 123

      assert {period0, _list} = result.debts |> List.pop_at(0)

      assert period0 == %{
               principal: 28.57,
               start_date: "2019-04-18",
               end_date: "2019-04-21"
             }

      assert {period1, _list} = result.debts |> List.pop_at(1)

      assert period1 == %{
               principal: 38.57,
               start_date: "2019-04-22",
               end_date: nil
             }
    end
  end
end
