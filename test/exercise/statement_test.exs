defmodule Exercise.StatementTest do
  use ExUnit.Case, async: true
  import Exercise.DataHelpers

  alias Exercise.Statement

  describe "cast_and_validate" do
    test "validates params keys presence" do
      invalid_params = %{invalid_key: "invalid_value"}

      assert Statement.cast_and_validate(invalid_params) ==
               {:error,
                 ["Invalid params. account_number, start_date and end_date are required."]}
    end

    test "validates account number is only number" do
      invalid_params = %{
        "account_number" => "xxx",
        "start_date" => "2019-04-15",
        "end_date" => "2019-04-15"
      }

      assert Statement.cast_and_validate(invalid_params) ==
        {:error, ["Invalid account number"]}
    end

    test "validates date format" do
      invalid_params = %{
        "account_number" => "123",
        "start_date" => 20_190_415,
        "end_date" => 20_190_415
      }

      assert Statement.cast_and_validate(invalid_params) ==
        {:error, ["Invalid end date format", "Invalid start date format"]}
    end

    test "validates date is valid" do
      invalid_params = %{
        "account_number" => "123",
        "start_date" => "2019-02-31",
        "end_date" => "2019-02-31"
      }

      assert Statement.cast_and_validate(invalid_params) ==
        {:error, ["Invalid end date", "Invalid start date"]}
    end

    test "validates date is required" do
      invalid_params = %{
        "account_number" => "123",
        "start_date" => nil,
        "end_date" => nil
      }

      assert Statement.cast_and_validate(invalid_params) ==
        {:error, ["Invalid end date format", "Invalid start date format"]}
    end

    test "validates date period range is valid" do
      invalid_params = %{
        "account_number" => "123",
        "start_date" => "2019-05-01",
        "end_date" => "2019-04-01"
      }

      assert Statement.cast_and_validate(invalid_params) ==
        {:error, ["Start date can't be greater than end date"]}
    end
  end

  describe "balances_by_date" do
    test "returns empty dates to account without any operations" do
      start_date = Date.utc_today() |> Date.add(-30)
      end_date   = Date.utc_today()

      operations = [
        build_operation(321, "Withdrawal", -180.0, end_date),
        build_operation(321, "Deposit", 1000, start_date)
      ]

      statement = %Statement{
        account_number: 123, start_date: start_date, end_date: end_date
      }

      assert result = Statement.balances_by_date(statement, operations)

      assert result.account_number == 123
      assert result.start_date == start_date
      assert result.end_date == end_date
      assert result.dates == []
    end

    test "returns the operations of each day and the balance" do
      start_date = Date.utc_today() |> Date.add(-30)
      start_date1 = start_date |> Date.add(1)
      start_date2 = start_date |> Date.add(2)
      end_date   = Date.utc_today()

      operations = [
        build_operation(123, "Withdrawal", -180.0, end_date),
        build_operation(123, "Deposit", 1000.0, start_date1),
        build_operation(123, "Purchase on Amazon", -3.34, start_date2),
        build_operation(123, "Purchase on Uber", -45.23, start_date2),
        build_operation(123, "Credit on future", 199.0, end_date |> Date.add(1)),
        build_operation(321, "Deposit", 199.0, start_date)
      ]
      statement = %Statement{
        account_number: 123, start_date: start_date, end_date: end_date
      }

      assert result = Statement.balances_by_date(statement, operations)

      assert result.account_number == 123
      assert result.start_date == start_date
      assert result.end_date == end_date

      assert Enum.count(result.dates) == 3 # start_day+1, start_day+2, end_date

      assert {date0, _list} = result.dates |> List.pop_at(0)
      assert date0 == %{
        date: start_date1,
        operations: [
          %{description: "Deposit", amount: "1000.00"}
        ],
        balance: "1000.00"
      }

      assert {date1, _list} = result.dates |> List.pop_at(1)
      assert date1 == %{
        date: start_date2,
        operations: [
          %{description: "Purchase on Amazon", amount: "-3.34"},
          %{description: "Purchase on Uber", amount: "-45.23"}
        ],
        balance: "951.43"
      }

      assert {date2, _list} = result.dates |> List.pop_at(2)
      assert date2 == %{
        date: end_date,
        operations: [
          %{description: "Withdrawal", amount: "-180.00"}
        ],
        balance: "771.43"
      }
    end
  end
end
