defmodule Exercise.MemoryServerTest do
  use ExUnit.Case, async: true
  import Exercise.DataHelpers

  alias Exercise.Operation
  alias Exercise.MemoryServer

  setup :memory_server_clear

  test "holds operations state in memory" do

    credit_operation = %Operation{
      account_number: 123,
      description: "Deposit",
      amount: 1000.0,
      date: ~D[2019-04-15]
    }

    debit_operation = %Operation{
      account_number: 123,
      description: "Purchase on Amazon",
      amount: -3.34,
      date: ~D[2019-04-16]
    }

    assert MemoryServer.operations() == []

    assert MemoryServer.add_operation(credit_operation) == credit_operation
    assert MemoryServer.add_operation(debit_operation) == debit_operation
    assert MemoryServer.operations() == [debit_operation, credit_operation]
  end
end
