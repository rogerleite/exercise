defmodule Exercise.OperationTest do
  use ExUnit.Case, async: true

  alias Exercise.Operation

  describe "cast_and_validate" do
    test "validates params keys presence" do
      invalid_params = %{invalid_key: "invalid_value"}

      assert Operation.cast_and_validate(invalid_params) ==
               {:invalid_operation,
                ["Invalid params. account_number, description, amount and date are required."]}
    end

    test "validates account number is only number" do
      invalid_params = %{
        "account_number" => "xxx",
        "description" => "Deposit",
        "amount" => 1000,
        "date" => "2019-04-15"
      }

      assert Operation.cast_and_validate(invalid_params) ==
               {:invalid_operation, ["Invalid account number"]}
    end

    test "validates amount is only number" do
      invalid_params = %{
        "account_number" => "123",
        "description" => "Deposit",
        "amount" => "xxx",
        "date" => "2019-04-15"
      }

      assert Operation.cast_and_validate(invalid_params) ==
               {:invalid_operation, ["Invalid amount"]}
    end

    test "validates date format" do
      invalid_params = %{
        "account_number" => "123",
        "description" => "Deposit",
        "amount" => 1000,
        "date" => 20_190_415
      }

      assert Operation.cast_and_validate(invalid_params) ==
               {:invalid_operation, ["Invalid date format"]}
    end

    test "validates date is valid" do
      invalid_params = %{
        "account_number" => "123",
        "description" => "Deposit",
        "amount" => 1000,
        "date" => "2019-02-31"
      }

      assert Operation.cast_and_validate(invalid_params) == {:invalid_operation, ["Invalid date"]}
    end

    test "validates date is required" do
      invalid_params = %{
        "account_number" => "123",
        "description" => "Deposit",
        "amount" => 1000,
        "date" => nil
      }

      assert Operation.cast_and_validate(invalid_params) ==
               {:invalid_operation, ["Invalid date format"]}
    end

    test "validates description is required" do
      invalid_params = %{
        "account_number" => "123",
        "description" => nil,
        "amount" => 1000,
        "date" => "2019-04-15"
      }

      assert Operation.cast_and_validate(invalid_params) ==
               {:invalid_operation, ["description is required"]}
    end

    test "builds a valid credit operation" do
      valid_params = %{
        "account_number" => "123",
        "description" => "Deposit",
        "amount" => 1000,
        "date" => "2019-04-15"
      }

      assert {:ok, operation} = Operation.cast_and_validate(valid_params)
      assert operation.account_number == 123
      assert operation.description == "Deposit"
      assert operation.amount == 1000.0
      assert operation.date == ~D[2019-04-15]
    end

    test "builds a valid debit operation" do
      valid_params = %{
        "account_number" => "123",
        "description" => "Purchase on Amazon",
        "amount" => -3.34,
        "date" => "2019-04-16"
      }

      assert {:ok, operation} = Operation.cast_and_validate(valid_params)
      assert operation.account_number == 123
      assert operation.description == "Purchase on Amazon"
      assert operation.amount == -3.34
      assert operation.date == ~D[2019-04-16]
    end
  end
end
