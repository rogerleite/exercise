defmodule Exercise.BalanceTest do
  use ExUnit.Case, async: true
  import Exercise.DataHelpers

  alias Exercise.Balance

  describe "current_balance" do
    test "validates account number is number" do
      assert Balance.current_balance("xxx", []) == {:error, ["Invalid account number"]}
    end

    test "validates account number without operations" do
      assert Balance.current_balance(123, []) ==
               {:ok, %{account_number: 123, balance: 0.0}}
    end

    test "supports account number as string" do
      assert Balance.current_balance("123", []) ==
               {:ok, %{account_number: 123, balance: 0.0}}
    end

    test "returns sum of all operations until today" do
      operations = [
        build_operation(123, "Deposit", 1000, ~D[2019-04-15]),
        build_operation(123, "Purchase on Amazon", -3.34, ~D[2019-04-16]),
        build_operation(123, "Purchase on Uber", -45.23, ~D[2019-04-16]),
        build_operation(123, "Withdrawal", -180.0, Date.utc_today()),
        build_operation(123, "Credit on future", 199.0, Date.utc_today() |> Date.add(1)),
        build_operation(321, "Deposit", 199.0, ~D[2019-04-15])
      ]

      # 1000.00 - 3.34 - 45.23 - 180.00 = 771.43
      assert Balance.current_balance(123, operations) ==
               {:ok, %{account_number: 123, balance: 771.43}}
    end
  end
end
