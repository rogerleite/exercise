# Exercise

## How to build and run

To run application:

  * Install erlang 21.1
  * Install elixir 1.8.1
  * Git clone: https://gitlab.com/rogerleite/exercise.git
  * Install dependencies with `mix deps.get`
  * Start Phoenix endpoint with `mix phx.server`

The server will be available at `http://localhost:4000`.
See below on [Solution description section](#solution-description), how to use API.

To run tests:

  * Install dependencies with `mix deps.get`
  * Start tests with `mix test`

To follow CI's results, check https://gitlab.com/rogerleite/exercise/pipelines.

## Solution description

* GenServer to hold data on memory
* Phoenix framework to handle web calls
* Gitlab's CI to run tests on each pushed commit

### 1) Add operations on account

`POST  /operations`

Request json specification:

* `account_number`: Integer and required.
* `amount`: Float and required. Supports negative and positive values.
* `date`: String and required. Supports ISO 8601 format. Ex: "2019-04-15".
* `description`: String and required.

Example of json for credit operation:

```
{
    "account_number": 123,
    "amount": 1000.0,
    "date": "2019-04-15",
    "description": "Deposit"
}
```

Example of json for debit operation:
```
{
    "account_number": 123,
    "amount": -3.34,
    "date": "2019-04-16",
    "description": "Purchase on Amazon"
}
```

Example to call API:

```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"account_number":123,"description":"Deposit","amount":1000,"date":"2019-04-15"}' \
  http://localhost:4000/operations
```

Example of success response (200 ok):

```
{
    "account_number": 123,
    "amount": "1000.00",
    "date": "2019-04-15",
    "description": "Deposit"
}
```

Example of validation response (422 unprocessable entity):

```
{
    "errors": [
        "Invalid account number"
    ]
}
```

You can also use `priv/example_operations.sh` to create operations of exercise on account number 123.

### 2) Get current balance

`GET   /balance/:account_number`

Url params specification:

* `account_number`: Integer and required.

Example to call API:

```
curl --header "Content-Type: application/json" http://localhost:4000/balance/123
```

Example of success response (200 ok):

```
{
  "account_number": 123,
    "balance": 61.43
}
```

Example of validation response (422 unprocessable entity):

```
{
    "errors": [
        "Invalid account number"
    ]
}
```

### 3) Get the bank statement

`GET   /statement/:account_number`

Url params specification:

* `account_number`: Integer and required.

Query string params specification:

* `start_date`: String and required. Supports ISO 8601 format. Ex: "2019-04-15".
* `end_date`: String and required. Supports ISO 8601 format. Ex: "2019-04-15".

Example to call API:

```
curl --header "Content-Type: application/json" \
  'http://localhost:4000/statement/123?start_date=2019-04-15&end_date=2019-05-01'
```

Example of success response (200 ok):

```
{
    "account_number": 123,
    "dates": [
        {
            "balance": "1000.00",
            "date": "2019-04-15",
            "operations": [
                {
                    "amount": "1000.00",
                    "description": "Deposit"
                }
            ]
        },
        {
            "balance": "951.43",
            "date": "2019-04-16",
            "operations": [
                {
                    "amount": "-45.23",
                    "description": "Purchase on Uber"
                },
                {
                    "amount": "-3.34",
                    "description": "Purchase on Amazon"
                }
            ]
        },
        {
            "balance": "771.43",
            "date": "2019-04-17",
            "operations": [
                {
                    "amount": "-180.00",
                    "description": "Withdrawal"
                }
            ]
        }
    ],
    "end_date": "2019-05-01",
    "start_date": "2019-04-15"
}
```

Example of validation response (422 unprocessable entity):

```
{
    "errors": [
        "Invalid account number"
    ]
}
```

### 4) Compute periods of debt

`GET   /debt_period/:account_number`

Url params specification:

* `account_number`: Integer and required.

Example to call API:

```
curl --header "Content-Type: application/json" http://localhost:4000/debt_period/123
```

Example of success response (200 ok):

```
{
    "account_number": 123,
    "debts": [
        {
            "end_date": "2019-04-21",
            "principal": 28.57,
            "start_date": "2019-04-18"
        },
        {
            "end_date": "2019-04-24",
            "principal": 38.57,
            "start_date": "2019-04-22"
        }
    ]
}
```

Example of validation response (422 unprocessable entity):

```
{
    "errors": [
        "Invalid account number"
    ]
}
```
